Généralités
========

Cette section définit quels sont les objectifs, les composants et le fonctionnement basique du couloir de MEP

Objectifs
--------

Le couloir de MEP est l'outil de l'Operateur pour réaliser les déploiements automatisés et sécurisés de l'IQ à la Prod.

Les objectifs du couloir sont : 

- Proposer aux pôles DSI un outil fiable et sécurisé pour les rendre autonomes sur leurs changements
- Assurer la sécurité, la qualité et l'évaluation des impacts de changements délivrés en production dans un contexte d'agilité croissante.

Le couloir propose deux modes de fonctionnement : 

- Mode continu : l'intégralité du changement est automatisée
- Mode délégué : l'outil fournit des droits au projet pour réaliser lui même son déploiements

Dans les deux cas : 

- Le changement doit impérativement passer par un environnement hors production au préalable
- Une série de tests automatisés est réalisée pour assurer que les exigeances de sécurité et d'opérabilité sont remplies


Composants
--------

Le couloir de MEP est assuré grâce à plusieurs applications et leurs composants

- MySI / Peach V2 : 

- Adaje : la solution de déploiement continu basée sur : 

    - Vault-API
    - Adaje-Workflow
    - Adaje-Deploy
    - Vault Secure
    - Reposit'OI / Ansible

- Stevia : la solution de testing automatisée basée sur : 

    - Portail Stevia
    - Adaje-Ano

Fonctionnement
--------

