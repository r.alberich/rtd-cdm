Template
========



Généralités : 
- Qu'est ce que le couloir de MEP (dont les 2 modes)
- Les applications (synthèse + liens)
- Comment ça marche (workflow bout en bout)

Faire une première MEP
    Choisir le mode de déploiement
    Prérequis
        Compatibilité
            Types de VM
            Types d'OS
            Zones Réseaux
            Cloisonnement
            Taille de la MEP
        Livrables
            Créer les dépots Reposit'OI
            Migrer les packages du Nexus vers Reposit'OI (Optionnel)
            Créer les jobs de livraison dans la PIC Projet
            Pousser la documentation dans un dépot générique
        Testing 
            Configurer votre bugtracker dans Adaje-Ano
            Fournir les inventaires des environnement à Stevia
            Créer les campagnes de test dans Stevia
        Déploiement
            Initialiser les environnements dans Jenkins
            Récupérer les clés SSH pour votre git pic (Optionnel)
            Initialiser les secrets dans Vault
            Préparer le plan de déploiement
        Changement
            Accès à Peach V2 et paramétrage
                Config de l'application
                Config des acteurs
            Accès à Idatha Expert
            Accès à Mattermost
            Accès au changelog gitlab

    Réaliser et suivre le déploiement
        Déclarer la RFC dans Peach V2
        Déroulé du workflow
            Initialisation
            Préparation
            Pré-déploiement
            Déploiement
            Tests
            PostActions
            Environnement suivant
            Communications MySI
        Suivi du déploiement
            Dans Peach V2
            Dans Mattermost
            Dans Idatha Expert

    Garder votre référentiel à jour
        Bonnes pratiques de mise à jour du référentiel?

    Débuggage et amélioration continue
        Anomalies dans votre bugtracker
        Consultation des logs du déploiement dans IdathaLog
        Consultation des logs de test dans Stevia
        Engagements et dérogations sur les tests
        
    Erreurs connues
        1..n

Autres : //porter ici les documentations spécifiques "coeur de process" des outils du couloir
    Stevia
        Configurer des tests Projet
        Consulter les tests pour mon application
        Comparer les résultats de deux changements
        Utiliser Stevia hors couloir
    MySI 
        Roadmap des changements


TODO :
- IAAS (non utilisés en standard par le couloir)
- 

Comment faire une MEP qui contient
- Modif Ctlm
- Modif patrol
- Modif FHM


