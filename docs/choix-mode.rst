Choix du mode de déploiement
=============================

Le couloir de MEP propose deux modes de déploiement : 

- Mode continu : l'intégralité du changement est automatisée
- Mode délégué : l'outil fournit des droits au projet pour réaliser lui même son déploiements

Dans les deux cas : 

- Le changement doit impérativement passer par un environnement hors production au préalable
- Une série de tests automatisés est réalisée pour assurer que les exigeances de sécurité et d'opérabilité sont remplies

**Le choix du mode de changement peut varier d'un changement à l'autre.**

Conditions de choix : 
---------------------

Il est recommandé de basculer en mode automatisé si : 

- Le changement concerné est simple
- Votre application est déjà déployée via adaje
- Le changement concerné n'implique **aucun** geste manuel

Il est reccomandé d'utiliser le mode délégué si : 
- Tout ou partie de vos changement nécessitent des gestes manuels
- Votre changement implique des changement d'architecture majeurs

