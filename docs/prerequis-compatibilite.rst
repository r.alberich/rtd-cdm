Compatibilité
==============

Les pré-requis cités ci-dessous sont valables pour le périmètre d’un changement et non d’une application.
Ils évolueront au fur et à mesure du développement du couloir de MEP.


Types de VM ? 
--------------

Seules les VM Virtuose sont actuellement compatibles.

.. note::

    Concernant les VM Gate et du Cloud Enedis, des solutions sont à l'étude.

Types d’OS
------------

Le Couloir de Mep est compatible avec : 

- Linux
- Aix 

.. note::

    Le déploiement sur serveurs Windows sera bientôt effectif

Zones réseaux
--------------

Zones réseaux compatibles : 

- ZHB
- DMZ
- ZSA

.. note:: 
    
    L’implémentation de la solution en ZSE est à l’étude. 


Cloisonnement de la MEP
------------------------- 

Le Couloir de Mep fonctionne pour un seul NNA (changement mono-application).
Les déploiements avec actions (exemple : blocage de flux) sur une autre application, ou les mises en production multi-application, seront mis en œuvre dans une version ultérieure du couloir de MEP. 

Taille de la MEP
-----------------

Le Couloir de Mep en mode automatique est particulièrement adapté aux changements type AVD (changement simple et dont le retour arrière est maitrisé) ou patches (War / SQL).

La solution de sauvegarde de sécurité actuelle est un snapshot des VM concernées.

.. warning::

    Echangez avec votre RT à ce sujet si votre changement impacte plus de 5 VM applicatives.