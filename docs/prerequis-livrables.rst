Livrables
==========

Créer les dépots Reposit'OI
-----------------------------

TODO : Comment demander ces repo??

L’espace de livraison officiel pour tous les packages est Artifactory. 

Les instances Artifactory sont gérées par le service Reposit'OI.

L'instance principale est accessible via : http://artifactory-principale.enedis.fr/artifactory 

La structure du dépot *${nna}-generic-stages* doit contenir : 

-  un espace *packages* pour les packages

    - un espace *packages/souches* pour les souches Prodis : *packages/souches/$techno/$techno_version/$package*

- un espace *indus* pour les plans de déploiements : *indus/$version/deploy/plan.json* 

- un espace documentaire : *doc/$version/RELEASE_NOTES.MD*

.. note::

    Afin d'assurer l'intégrité des livrables entre les environnements, un système de promotion sera implémenté
    
    Les dépots de type *stage* seront a accessibles en lecture/écriture par le projet

    Les dépôts de type *releases* seront accessibles uniquement en lecture. Ces derniers contiendraient les artifacts promus à la fin de l’IQ (ou de la PreProd en l'absence d'IQ)



Migrer les packages Nexus
--------------------------

Tout projet utilisant le service Nexus OI doit migrer ses livrables dans Reposit'OI.

Créer les jobs de livraison dans la PIC Projet.
-----------------------------------------------

Pousser la documentation dans un dépot générique.
-------------------------------------------------


