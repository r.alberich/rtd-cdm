Testing
=========

Configurer le bugtracker dans Adaje-Ano : 
------------------------------------------

Adaje-ano est le composant Adaje qui communique techniquement avec les bugtrackers projets.

La connexion du bugtracker est obligatoire pour le raccordement au couloir de MEP.

Adaje-Ano associe à chaque NNA une configuration de bugtracker : 
- Le type de bugtracker (jira, gitlab, ...)
- l'url
- les credentials
- une configuration spécifique si nécessaire

-----------

Pour Configurer le bugtracker il faut : 

- Fournir l'url du bugtracker
- Fournir un compte lecture / ecriture à votre projet dans le bugtracker
- Spécifier les configurations spécifiques souhaitées dans le remplissage des fiches générées

    Exemples : 
        - Etat custom dans le workflow
        - Tags
        - Type de fiche à utiliser (bug/issue recommandé)
    
.. note::
    Si vous utilisez une instance bugtracker partagée, vérifiez avec vos gestionnaires qu'un compte Adaje n'existe pas déjà

Ces éléments doivent être transmis à la bal ADAJE : dsi-audes-applications-adaje@enedis.fr

Pour valider la bonne configuration du bugtracker, nous générons une fiche qui peut ensuite être abandonnée.

.. warning::
    Pour chaque type de bugtracker différent, les méthodes de gestion doivent être adaptées à l'API du bugtracker.

    Les types de bugtrackers actuellement connectés sont : 

    - JIRA
    - Gitlab

    Les types de bugtrackers identifiés comme connectables mais non implémentés sont : 
    
    - CA Agile
    - Redmine
    - Target Process

    Si votre bugtracker fait partie de cette liste, pensez à anticiper votre demande.
    Si votre bugtracker ne fait partie d'aucune de ces deux listes, prenez contact avec nous ! dsi-audes-applications-adaje@enedis.fr

Vérifier les inventaires : 
---------------------------

Afin de déterminer les tests à réaliser sur votre application, Stevia nécessite un inventaire : 

- Des serveurs concernés par votre application
- De chaque composant utilisé

Pour définir votre besoin, remplissez le `fichier suivant
<pj/modele-jeu_de_donnees_stevia-nouveau_projet.xlsx>`_ et transmettez le à la BAL dsi-audes-apps-stevia@mix.local 

.. warning::
    Le fichier doit être rempli par environnement ! (3 fichiers ou 3 onglets, comme vous voulez!)

.. note::
    Si votre application est déjà passée par une étape d'IQ standard, vous pouvez demander un extract de ces données pour l'env. d'IQ à la BAL Stevia


Créer les campagnes de test : 
-----------------------------

Sur la base des inventaires validés RT & POA, la création des campagnes de test opérabilité et sécurité sont crées par l'équipe STEVIA.

Cette création est habituellement réalisée sous 24h et consultable une fois réalisée sur le portail Stevia : https://stevia.enedis.fr
