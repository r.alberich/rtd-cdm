Prérequis
==========

Cette section détaille les prérequis et les contraintes de compatibilité du couloir de MEP.

Elle doit être suivie attentivement et remplie de manière exhaustive pour l'arrivée de toute nouvelle application.

.. toctree::
   :maxdepth: 2
   
   prerequis-compatibilite
   prerequis-livrables
   prerequis-testing
   prerequis-deploiement
   prerequis-changement

